#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:32621902:c507af87309c12ff2fb7be43d6cc576e2527df5b; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:26461514:72bd879a0dc4ba4a55bd25cce74d5d2c660c1927 EMMC:/dev/block/bootdevice/by-name/recovery c507af87309c12ff2fb7be43d6cc576e2527df5b 32621902 72bd879a0dc4ba4a55bd25cce74d5d2c660c1927:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
